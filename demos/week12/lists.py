# list is built-in
x = [0, 1, 2, 3, 3]

x[2] == 2 # access via [] index operator, zero-based

x.insert(0, 5) # index, value
# x == [5, 0, 1, 2, 3, 3]

# remove by index - returns value
x.pop(0) # returns 5
# x == [0, 1, 2, 3, 3]

x = [0, 1, 2, 'three'] # can contain arbitrary types!

# access from the back with negative indices
x[-2] == 2

# adding lists concatenates them
x += [4, 5, 6] # x == [0, 1, 2, 'three', 4, 5, 6]

# slicing [start:stop + 1) (stop + 1 is excluded!)
x[1:4] == [1, 2, 'three']

# slicing [start: until end]
x[1:] == [1, 2, 'three', 4, 5, 6]

# slicing [beginning: until end - 1]
x[0:-1] == [0,1, 2, 'three', 4, 5]

# slicing with a stride [start:stop + 1:step)
x[0:7:2] == x[::2] == [0, 2, 4, 6]

# reverse slicing
x[-1:0:-2] == [6, 4, 2]
x[::-2] == [6, 4, 2, 0]
