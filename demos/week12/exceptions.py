import sys
from numbers import Number

def func4posnuminput(x):
    """Function taking only numeric and positive input!"""
    if isinstance(x, Number):
        if x < 0: # note that this raise TypeError for Complex x!
            raise ValueError
    else:
        raise TypeError
    raise SyntaxError

X = [1, -1, 2.71, -2.71, 1j, abs(1.j), '1.9']
for x in X:
    try:
        func4posnuminput(x)
        print("x = {:>5} raised no errors!".format(x))
    except ValueError: # catch ValueError exception
        print("x = {:>5} raised a ValueError".format(x))
    except TypeError:  # catch TypeError  exception
        print("x = {:>5} raised a TypeError".format(x))
    except: # catch any other exception
        # print the exception
        print("Unexpected error:", sys.exc_info()[0])
        raise # re-raise
