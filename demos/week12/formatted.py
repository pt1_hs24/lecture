import math

which = "f-string"
# which = "str-format"
# which = "old-format"

print("Table with default format:")
print("i", "PI**i", "PI**(2*i)", "PI**(3*i)")
for i in range(-5, +5+1):
    print(i, math.pi**(i), math.pi**(2*i), math.pi**(3*i))
print(80*"#")

print("Table with some formatting using " + which + " method:")
if which == "f-string":
    print(f"{'i':5s} {'PI**i':15s} {'PI**(2*i)':15s} {'PI**(3*i)':15s}")
    for i in range(-5, +5+1):
        print(f"{i:5d} {math.pi**(  i):15g} {math.pi**(2*i):15g} "
              f"{math.pi**(3*i):15g}")
elif which == "str-format":
    print("{:5s} {:15s} {:15s} {:15s}".format("i", "PI**i", "PI**(2*i)",
                                          "PI**(3*i)"))
    for i in range(-5, +5+1):
        print("{:5d} {:15g} {:15g} {:15g}".format(i, math.pi**(  i),
                                                     math.pi**(2*i),
                                                     math.pi**(3*i)))
elif which == "old-format":
    print("%5s %15s %15s %15s" % ("i", "PI**i", "PI**(2*i)", "PI**(3*i)"))
    for i in range(-5, +5+1):
        print("%5d %15g %15g %15g" % (i, math.pi**(  i),
                                         math.pi**(2*i),
                                         math.pi**(3*i)))
print(80*"#")

print("Table with more formatting using " + which + " method:")
if which == "f-string":
    print(f"{'i':5s} {'PI**i':15s} {'PI**(2*i)':15s} {'PI**(3*i)':15s}")
    for i in range(-5,+5+1):
        print(f"{i:5d} {math.pi**(  i):15.8e} {-math.pi**(2*i):15.8e} "
              f"{-math.pi**(3*i):15.8e}")
elif which == "str-format":
    print("{:5s} {:15s} {:15s} {:15s}".format("i", "PI**i", "PI**(2*i)",
                                              "PI**(3*i)"))
    for i in range(-5,+5+1):
        print("{:5d} {:15.8e} {:15.8e} {:15.8e}".format(i, math.pi**(  i),
                                                          -math.pi**(2*i),
                                                          -math.pi**(3*i)))
elif which == "old-format":
    print("%5s %15s %15s %15s" % ("i", "PI**i", "PI**(2*i)", "PI**(3*i)"))
    for i in range(-5,+5+1):
        print("%5d %15.8e %15.8e %15.8e" % (i, -math.pi**(  i),
                                               -math.pi**(2*i),
                                               -math.pi**(3*i)))
print(80*"#")
