#include <iostream>

double function1() {
  int i;
  double retval = 0.;
  for (i=1; i<10000000; ++i) {
    retval += 1./i;
  }
  return retval;
}

double function2() {
  int i;
  double retval = 0.;
  for (i=1; i<100000000; ++i) {
    retval += 1./(i + 1.);
  }
  return retval;
}

void function3() {
  return;
}

int main() {
  int i;
  std::cout << "function1 result: " << function1() << std::endl;
  std::cout << "function2 result: " << function2() << std::endl;
  if (false) function3();
}
