// compile with avx enabled
// E.g., gnu/clang: g++ -mavx intrinsic.cpp
#include <iostream>
#include <vector>
#include <immintrin.h> // intrinsics header

double dot(int n, double* a, double* b) {
  double res = 0.;
  for (int i = 0; i < n; ++i) {
    res += a[i] * b[i];
  }
  return res;
}

double dot_vec(int n, const double* a, const double* b) {
  int i;
  // initialize to zero a tmp vector tmp = [0., 0., 0., 0.]
  __m256d tmp = _mm256_setzero_pd();
  for (i = 0; i <= n - 4; i += 4) {
    // load (unaligned) array a into va = [a[i+0], a[i+1], a[i+2], a[i+3]]
    __m256d va = _mm256_loadu_pd(&a[i]);
    // load (unaligned) array b into vb = [b[i+0], b[i+1], b[i+2], b[i+3]]
    __m256d vb = _mm256_loadu_pd(&b[i]);
    // multiply and add
    // tmp += [a[i+0]*b[i+0], a[i+1]*b[i+1], a[i+2]*b[i+2], a[i+3]*b[i+3]]
    tmp = _mm256_add_pd(tmp, _mm256_mul_pd(va, vb));
  }

  // sum up the partial results
  double partial_sum[4];
  _mm256_storeu_pd(partial_sum, tmp); // store the partial results in an array
  double res = partial_sum[0] + partial_sum[1]
             + partial_sum[2] + partial_sum[3];

  // handle the remaining elements if n is not a multiple of 4
  for (; i < n; ++i) {
    res += a[i] * b[i];
  }

  return res;
}


int main() {
  std::vector<double> a = {1., 2., 3., 4.};
  std::vector<double> b = {1., 2., 3., 4.};
  
  std::cout << "dot    (a, b) = " << dot    (4, &a[0], &b[0]) << std::endl;
  std::cout << "dot_vec(a, b) = " << dot_vec(4, &a[0], &b[0]) << std::endl;
}
