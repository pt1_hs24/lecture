#ifndef MIN_HPP
#define MIN_HPP

template <typename T, typename U> struct min_type { };

// partial specialization
template <typename T> struct min_type<T,T> { typedef T type; };

// full specialization
template <> struct min_type<float,double> { typedef double type; };
template <> struct min_type<double,float> { typedef double type; };
template <> struct min_type<float,int> { typedef float type; };
template <> struct min_type<int,float> { typedef float type; };

/* Same as above, but with the C++11 using declaration.
// partial specialization
template <typename T> struct min_type<T, T> { using type = T; };

// full specialization
template <> struct min_type<float, double> { using type = double; };
template <> struct min_type<double, float> { using type = double; };
template <> struct min_type<float, int> { using type = float; };
template <> struct min_type<int, float> { using type = float; };
*/


template <typename T, typename U>
typename min_type<T,U>::type min(T const& x, U const& y) {
  return x < y ? x : y;
}

#endif /* MIN_HPP */
