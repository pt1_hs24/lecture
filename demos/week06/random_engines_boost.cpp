#include <boost/random.hpp>

int main() {

  // Mersenne-twisters
  boost::mt11213b rng1;
  rng1.seed(42); // set seed
  boost::mt19937 rng2;

  // lagged Fibonacci generators
  boost::lagged_fibonacci607 rng3;
  boost::lagged_fibonacci1279 rng4;
  boost::lagged_fibonacci2281 rng5;

  // linear congruential generators
  boost::minstd_rand0 rng6;
  boost::minstd_rand rng7;

  // min & max numbers
  std::cout << "min: " << rng1.min() << '\n';
  std::cout << "max: " << rng1.max() << '\n';

  // generate 5 random numbers
  for (int i = 0; i < 5; ++i) {
    std::cout << "i = " << i << ": " << rng1() << '\n';
  }

  return 0;
}
