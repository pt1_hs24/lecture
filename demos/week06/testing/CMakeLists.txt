cmake_minimum_required(VERSION 3.15)

project(PT1_week06_testing_fibonacci)

set(CMAKE_CXX_STANDARD 11)

# use C++11 (globally)
set(CMAKE_CXX_STANDARD 11)          # set standard to C++11
set(CMAKE_CXX_STANDARD_REQUIRED ON) # requires C++11
set(CMAKE_CXX_EXTENSIONS OFF)       # disables extensions such as -std=g++XX

# setting warning compiler flags
add_compile_options(-Wall -Wextra -Wpedantic)

add_subdirectory(src)

option(BUILD_TESTS_CTEST "Build CTest tests." ON )
option(BUILD_TESTS_CATCH "Build Catch tests." OFF)

if (BUILD_TESTS_CTEST OR BUILD_TESTS_CATCH)
  enable_testing()
  if (BUILD_TESTS_CTEST) # Simple: Tests written in main
    add_subdirectory(test_in_main)
  endif()
  if (BUILD_TESTS_CATCH) # Advanced: Tests written with the Catch test framework
    add_subdirectory(test_with_catch)
  endif()
endif()
