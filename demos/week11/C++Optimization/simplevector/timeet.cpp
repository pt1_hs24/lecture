#include <iostream>
#undef HAVE_BOOST
#ifdef HAVE_BOOST
#include <boost/core/demangle.hpp>
#endif
#include "etvector.hpp"

int main() {
  const int N = 10000000;
  etvector<double> a(N), b(N), c(N), d(N);
  for (int i=0; i<100; ++i) {
    a = b + c + d;
    c[0] += i; // avoid loop inversion
  }
  std::cout << a[0] << std::endl;

#ifdef HAVE_BOOST
  std::cout << "type: " << boost::core::demangle(typeid(a + b + c).name()) << '\n';
#endif

}
