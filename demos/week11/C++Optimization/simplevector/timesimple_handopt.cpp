#include <iostream>
#include "simplevector.hpp"

int main() {
  const int N = 10000000;
  simplevector<double> a(N), b(N), c(N), d(N);
  for (int i=0; i<100; ++i) {
    for (int j=0; j<a.size(); ++j) {
      a[j] = b[j] + c[j] + d[j];
    }
  }
  std::cout << a[0] << std::endl;
}
