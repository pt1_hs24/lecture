#include <iostream>

// the general factorial
template<int N>
struct Factorial {
  // enum { value = N * Factorial<N-1>::value }; // "early" C++ no in-class
                                                 // static const initializer
  static int const value = N * Factorial<N-1>::value;
};

// the specialization that stops the recursion
template<>
struct Factorial<1> {
  // enum { value = 1 }; // "early" C++
  static int const value = 1;
};

// C++11 constexpr function
// Note that according the C++ standard the compiler can choose to evaluate the
// function either at compile- or run-time, unless a constant expression is
// required.
constexpr int factorial(int N) {
  return N <= 1 ? 1 : (N * factorial(N-1));
}

int main() {
  std::cout << "Factorial< 5>::value = " << Factorial< 5>::value << "\n";
  std::cout << "factorial(5)         = " << factorial(5)         << "\n";
  std::cout << "Factorial<11>::value = " << Factorial<11>::value << "\n";
  std::cout << "factorial(11)        = " << factorial(11)        << "\n";
}
