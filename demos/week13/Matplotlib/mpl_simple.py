import numpy as np
import matplotlib.pyplot as plt

# plot
x = np.linspace(-1.5*np.pi, +1.5*np.pi, 100)
plt.plot(x, np.sin(x), 'b-', label=r"$\sin(x)$")
plt.plot(x, np.cos(x), 'r-', label=r"$\cos(x)$")
plt.plot(x, np.tan(x), 'g-', label=r"$\tan(x)$")

# adjust lower and upper limits of x & y axes
plt.xlim(np.min(x), np.max(x))
plt.ylim(-np.pi, +np.pi)

# set axes lables
plt.xlabel(r"$x$")
plt.ylabel(r"$y$")

# further plot cosmetcs
plt.legend()
plt.title("Trigonometric functions")

# save the figure
plt.savefig("trigfun.pdf")

# show!
plt.show()
