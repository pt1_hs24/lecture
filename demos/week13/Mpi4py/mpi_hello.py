from mpi4py import MPI


if __name__ == "__main__":
    # get COMMON WORLD communicator, size & rank
    comm = MPI.COMM_WORLD
    size = comm.Get_size()
    rank = comm.Get_rank()
    proc = MPI.Get_processor_name()

    # hello world!
    print(f"Hello world from processor {proc}, rank {rank} out of {size} "
          f"processes")
