#!/bin/bash
#SBATCH -n 4
#SBATCH --time=4:00:00
#SBATCH --job-name="mpi"

# Note: We will briefly discuss this in the last lecture (Xmas bonus)

(time -p mpirun python mpi_hello.py) 2>&1 | tee mpi4py.log

exit
