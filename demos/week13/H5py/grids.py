import numpy as np
import h5py as h5
import xml.etree.ElementTree as ET
from xml.dom import minidom


def generate_data(dim=2):
    """
    Generate a grid and some data on it.
    The generated grids are based on:

      Calhoun, Helzel, LeVeque, "Logically Rectangular Grids and Finite Volume
      Methods for PDEs ,in Circular and Spherical Domains", SIAM Review, 2008
    """
    # set grid dimensions
    nx = 43
    ny = 21
    nz = 44

    # generate grid, some point & cell data
    if ( dim == 2 ):
        # generate computational domain grid
        xc = np.linspace(-1., +1., nx+1)
        yc = np.linspace(-1., +1., ny+1)
        zc = np.linspace( 0.,  0., 1)
        [XC, YC, ZC] = np.meshgrid(xc, yc, zc)
 
        # generate physical domain grid
        r1 = 1.
        D = np.maximum(np.abs(XC),np.abs(YC))
        r = np.sqrt(XC**2 + YC**2)
        r = np.maximum(r, 1e-10)
        XP = r1*D*XC/r
        YP = r1*D*YC/r
        W = D**2
        XP = W*XP + (1. - W)*XC/np.sqrt(2)
        YP = W*YP + (1. - W)*YC/np.sqrt(2)
        ZP = ZC
 
        # generate some point data on physical domain grid
        PD = np.sin(np.pi*XP)*np.sin(np.pi*YP)
 
        # generate some cell  data on physical domain grid
        CD = np.sin(np.pi*XP[1:,1:,:])*np.sin(np.pi*YP[1:,1:,:])

    elif ( dim == 3 ):
        # generate computational domain grid
        xc = np.linspace(-1., +1., nx+1)
        yc = np.linspace(-1., +1., ny+1)
        zc = np.linspace(-1., +1., nz+1)
        [XC, YC, ZC] = np.meshgrid(xc, yc, zc)
 
        # generate physical domain grid
        r1 = 1.
        D = np.maximum(np.maximum(np.abs(XC),np.abs(YC)),np.abs(ZC))
        r = np.sqrt(XC**2 + YC**2 + ZC**2)
        r = np.maximum(r, 1e-10)
        XP = r1*D*XC/r
        YP = r1*D*YC/r
        ZP = r1*D*ZC/r
        W = D**2
        XP = W*XP + (1. - W)*XC/np.sqrt(3)
        YP = W*YP + (1. - W)*YC/np.sqrt(3)
        ZP = W*ZP + (1. - W)*ZC/np.sqrt(3)
 
        # generate some point data on physical domain grid
        PD = np.sin(np.pi*XP)*np.sin(np.pi*YP)*np.sin(np.pi*ZP)
 
        # generate some cell  data on physical domain grid
        CD = np.sin(np.pi*XP[1:,1:,1:]) \
           * np.sin(np.pi*YP[1:,1:,1:]) \
           * np.sin(np.pi*ZP[1:,1:,1:])


    return XP, YP, ZP, PD, CD


def write_xdmf(XP, YP, ZP, PD, CD, fname="test", time=np.pi):
    """
    Write data in HDF5 format and generate an XDMF file describing
    the data. The XDMF file can be read by visualization tools such
    as Paraview or VisIt.
    For more information on the XDMF format: http://www.xdmf.org
    """
    # get data size
    [nx,ny,nz] = np.shape(XP)

    # write hdf5 file
    with h5.File(fname + ".h5","w") as f:
        f.create_dataset("time", data=[time])
        f.create_dataset("XP", data=XP)
        f.create_dataset("YP", data=YP)
        f.create_dataset("ZP", data=ZP)
        f.create_dataset("PD", data=PD)
        f.create_dataset("CD", data=CD)

    # generate xdmf
    DimensionsPD = f"{nx} {ny} {nz}"
    DimensionsCD = f"{max(nx-1,1)} {max(ny-1,1)} {max(nz-1,1)}"
    Xdmf = ET.Element("Xdmf", {"Version": "3.0"})
    Domain = ET.SubElement(Xdmf, "Domain", {})
    Grid = ET.SubElement(Domain, "Grid",
                         {"Name": "mesh", "GridType": "Uniform"})
    Time = ET.SubElement(Grid, "Time", {"Value": f"{time}"})
    Topology = ET.SubElement(Grid, "Topology",
                             {"TopologyType": "3DSMesh", 
                              "Dimensions": DimensionsPD})
    Geometry = ET.SubElement(Grid, "Geometry", {"GeometryType": "X_Y_Z"})
    X = ET.SubElement(Geometry, "DataItem",
                      {"Dimensions": DimensionsPD,
                       "NumberType": "Float",
                       "Precision": "8",
                       "Format": "HDF"})
    X.text = fname + ".h5" + ":/XP"
    Y = ET.SubElement(Geometry, "DataItem",
                      {"Dimensions": DimensionsPD,
                       "NumberType": "Float",
                       "Precision": "8",
                       "Format": "HDF"})
    Y.text = fname + ".h5" + ":/YP"
    Z = ET.SubElement(Geometry, "DataItem",
                      {"Dimensions": DimensionsPD,
                       "NumberType": "Float",
                       "Precision": "8",
                       "Format": "HDF"})
    Z.text = fname + ".h5" + ":/ZP"
    Attribute = ET.SubElement(Grid, "Attribute",
                              {"Name": "PointData",
                               "AttributeType": "Scalar",
                               "Center": "Node"})
    DataItem = ET.SubElement(Attribute, "DataItem",
                             {"Dimensions": DimensionsPD,
                              "NumberType": "Float",
                              "Precision": "8",
                              "Format": "HDF"})
    DataItem.text = fname + ".h5" + ":/PD"
    Attribute = ET.SubElement(Grid, "Attribute",
                              {"Name": "CellData",
                               "AttributeType": "Scalar",
                               "Center": "Cell"})
    DataItem = ET.SubElement(Attribute, "DataItem",
                             {"Dimensions": DimensionsCD,
                              "NumberType": "Float",
                              "Precision": "8",
                              "Format": "HDF"})
    DataItem.text = fname + ".h5" + ":/CD"

    # write xdmf
    xmlstr = minidom.parseString(ET.tostring(Xdmf)).toprettyxml(indent="  ")
    with open(fname + ".xdmf", "w") as f:
        f.write(xmlstr)


if __name__ == "__main__":

    # 2D
    [XP, YP, ZP, PD, CD] = generate_data(dim=2)
    write_xdmf(XP, YP, ZP, PD, CD, fname="test2D")

    # 3D
    [XP, YP, ZP, PD, CD] = generate_data(dim=3)
    write_xdmf(XP, YP, ZP, PD, CD, fname="test3D")
