#include <hdf5.h> // hdf5 header

int main() {

  int const rank = 2; // rank of data, i.e. number of dimensions of data!
  hid_t file_id;      // file    identifier
  hid_t dataset_id;   // dataset identifier
  hid_t dataspace_id; // data    identifier
  hsize_t dims[rank]; // data dimensions
  herr_t status;      // error code

  // create & open a new file using default (creation & access) properties
  file_id = H5Fcreate("dataset.h5",  // file name
                      H5F_ACC_TRUNC, // file access flags
                      H5P_DEFAULT,   // file creation property list
                      H5P_DEFAULT);  // file access property list

  // create dataspace for the dataset
  dims[0] = 4;
  dims[1] = 6;
  dataspace_id = H5Screate_simple(rank,dims,NULL);

  // create the dataset using default properties
  dataset_id = H5Dcreate2(file_id,        // location identifier
                          "/dset",        // dataset name
                          H5T_NATIVE_INT, // datatype identifier
                          dataspace_id,   // dataspace identifier
                          H5P_DEFAULT,    // link creation property list
                          H5P_DEFAULT,    // dataset creation property list
                          H5P_DEFAULT);   // dataset access property list

  // close the dataset
  status = H5Dclose(dataset_id);

  // close the dataspace
  status = H5Sclose(dataspace_id);

  // close the file
  status = H5Fclose(file_id);

}
