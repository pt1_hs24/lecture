#include <hdf5.h> // hdf5 header
#include <iostream>

int main() {

  hid_t file_id;      // file    identifier
  hid_t dataset_id;   // dataset identifier
  herr_t status;      // error code

  // open existing file using default (creation & access) properties
  file_id = H5Fopen("dataset.h5",   // file name
                    H5F_ACC_RDONLY, // file access flags
                                    // (read only here)
                    H5P_DEFAULT);   // file access properties list
  // check if open succeeded (file_id negative means failure!)
  if ( file_id < 0 ) {
    std::cout << "Error reading dataset.h5!\n";
    return -1;
  }

  // open existing dataset
  dataset_id = H5Dopen2(file_id,      // location identifier
                        "/dset",      // dataset name
                        H5P_DEFAULT); // dataset access property list

  // write the dataset
  int dset_data[4][6];
  status = H5Dread(dataset_id,     // dataset identifier
                   H5T_NATIVE_INT, // memory datatype
                   H5S_ALL,        // memory dataspace (H5S_ALL means all)
                   H5S_ALL,        // dataset's dataspace in the file
                   H5P_DEFAULT,    // transfer property list
                   dset_data);     // buffer with data to be written

  // close the dataset
  status = H5Dclose(dataset_id);

  // close the file
  status = H5Fclose(file_id);

  // check read data
  int errs = 0;
  for(int i = 0; i < 4; ++i) {
    for (int j = 0; j < 6; ++j) {
      if ( dset_data[i][j] != 6*i + j ) ++errs;
    }
  }
  if ( errs > 0) std::cout << "Found " << errs << " errors!\n";

}
