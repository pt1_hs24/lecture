#include <hdf5.h> // hdf5 header

int main() {

  hid_t file_id; // file identifier
  herr_t status; // error code

  // create & open a new file using default (creation & access) properties
  file_id = H5Fcreate("file.h5",     // file name
                      H5F_ACC_TRUNC, // file access flags
                      H5P_DEFAULT,   // file creation property list
                      H5P_DEFAULT);  // file access property list

  // close the file
  status = H5Fclose(file_id); 

}
