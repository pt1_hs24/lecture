#include <iostream>
#include <fstream>
#include <string>
#include <vector>

int main() {

  int const N = 7;
  std::vector<double> data(N);
  for(int i = 0; i < N; ++i) {
    data[i] = i;
  }
    
  std::string fname("test.bin"); // file name

  // write to  file //
  // construct & connect to file
  std::ofstream fout(fname, std::fstream::binary);
  // check if it succeeded
  if (!fout) {
    std::cerr << "Error: open file for output failed!" << std::endl;
    return -1;
  }
  // write some stuff (ugly reinterpret_cast needed because write
  // takes a pointer to char!!!)
  fout.write(reinterpret_cast<char*>(&data[0]), data.size()*sizeof(double));
  // close the file
  fout.close(); 

  // read from file //
  // construct & connect to file
  std::ifstream fin(fname, std::fstream::binary);
  // check if it succeeded
  if (!fin) {
    std::cerr << "Error: open file for input failed!" << std::endl;
    return -1;
  }
  // read  some stuff ("ugly" reinterpret_cast needed because read
  // takes a pointer to char!!!)
  std::vector<double> data_in(N);
  fin.read(reinterpret_cast<char*>(&data_in[0])
          ,data_in.size()*sizeof(double));
  fin.close();

  // compare!
  int errs = 0;
  for(int i = 0; i < N; ++i) {
    if ( data_in[i] != data[i]) ++errs;
  }
  if ( errs > 0) std::cout << "Found " << errs << " errors!" << std::endl;

}
