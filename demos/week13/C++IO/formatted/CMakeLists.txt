cmake_minimum_required(VERSION 3.15)

project(PT1_week13_CppIO_formatted)

# use C++11 (globally)
set(CMAKE_CXX_STANDARD 11)          # set standard to C++11
set(CMAKE_CXX_STANDARD_REQUIRED ON) # requires C++11
set(CMAKE_CXX_EXTENSIONS OFF)       # disables extensions such as -std=g++XX

foreach(I 1 2 3 4)
  add_executable(formatted${I} formatted${I}.cpp)
endforeach(I)
add_executable(write2strings write2strings.cpp)
