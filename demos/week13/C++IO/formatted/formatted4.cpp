#include <iostream>
#include <iomanip>
#include <string>
#define _USE_MATH_DEFINES // for M_PI
#include <cmath>

std::ostream& print(std::ostream& out,int beg,int end) {

  // table description
  out << "i" << " "
      << "pow(M_PI,  i)" << " "
      << "pow(M_PI,2*i)" << " "
      << "pow(M_PI,3*i)" << std::endl;
  // write table
  for (int i = beg; i <= end; ++i) {
    out << i << " "
        << std::pow(M_PI,  i) << " "
        << std::pow(M_PI,2*i) << " "
        << std::pow(M_PI,3*i) << std::endl;
  }

  return out;
}

std::ostream& print_formatted(std::ostream& out,int beg,int end) {

  // table description
  out << std::setw( 5) << "i" << " "
      << std::setw(15) << "pow(M_PI,  i)" << " "
      << std::setw(15) << "pow(M_PI,2*i)" << " "
      << std::setw(15) << "pow(M_PI,3*i)" << std::endl;
  // write table
  for (int i = beg; i <= end; ++i) {
    out << std::setw( 5) << i << " "
        << std::setw(15) << std::pow(M_PI,  i) << " "
        << std::setw(15) << std::pow(M_PI,2*i) << " "
        << std::setw(15) << std::pow(M_PI,3*i) << std::endl;
  }

  return out;
}

std::ostream& print_more_formatted(std::ostream& out,int beg,int end) {

  // table description
  out << std::setw( 5) << "i" << " "
      << std::setw(15) << "pow(M_PI,  i)" << " "
      << std::setw(15) << "pow(M_PI,2*i)" << " "
      << std::setw(15) << "pow(M_PI,3*i)" << std::endl;
  // write table
  for (int i = beg; i <= end; ++i) {
    out << std::fixed;
    out << std::setw( 5) << i << " "
        << std::setw(15) << std::pow(M_PI,  i) << " "
        << std::setw(15) << std::pow(M_PI,2*i) << " "
        << std::setw(15) << std::pow(M_PI,3*i) << std::endl;
  }

  return out;
}

std::ostream& print_more2_formatted(std::ostream& out,int beg,int end) {

  // table description
  out << std::setw( 5) << "i" << " "
      << std::setw(15) << "pow(M_PI,  i)" << " "
      << std::setw(15) << "pow(M_PI,2*i)" << " "
      << std::setw(15) << "pow(M_PI,3*i)" << std::endl;
  // write table
  for (int i = beg; i <= end; ++i) {
    out << std::scientific;
    out << std::setw( 5) << i << " "
        << std::setw(15) << std::pow(M_PI,  i) << " "
        << std::setw(15) << std::pow(M_PI,2*i) << " "
        << std::setw(15) << std::pow(M_PI,3*i) << std::endl;
  }

  return out;
}

int main() {

  std::cout << "Table with default format:" << std::endl;
  print(std::cout,-5,+5);
  std::cout << std::string(80,'#') << std::endl;

  std::cout << "Table with some formatting:" << std::endl;
  print_formatted(std::cout,-5,+5);
  std::cout << std::string(80,'#') << std::endl;

  std::cout << "Table with more formatting:" << std::endl;
  print_more_formatted(std::cout,-5,+5);
  std::cout << std::string(80,'#') << std::endl;

  std::cout << "Table with even more formatting:" << std::endl;
  print_more2_formatted(std::cout,-5,+5);
  std::cout << std::string(80,'#') << std::endl;

}
