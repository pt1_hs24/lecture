#include <iostream>

double square(double x) {
  return x*x;
}

int main() {
  std::cout << square(5.)
            << std::endl;
  return 0;
}
