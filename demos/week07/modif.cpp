#include <algorithm>
#include <cmath> // for abs
#include <functional>
#include <iostream>
#include <list>
#include <numeric>
#include <random> // c++11
#include <vector>

template <typename C>
void print(std::string name,C const& cont) {
  std::cout << name << " =";
  for (typename C::const_iterator it = cont.begin(); it != cont.end(); ++it) {
    std::cout << " " << *it;
  }
  std::cout << std::endl;
}

class rng {
  public:
    rng(int seed=666) : seed_(seed), dist_(-1., +1.) {
      e_.seed(seed_); // set seed
    }
    double operator()() { return dist_(e_); }
  private:
    int seed_;                                    // seed
    std::default_random_engine e_;                // random number engine
    std::uniform_real_distribution<double> dist_; // uniform distribution
};

int main() {

  std::vector<int> vec1(10); // vector with 10 ints
  std::vector<int> vec2;     // empty vector

  // fill vec1 with zeros
  std::fill(vec1.begin(), vec1.end(), 0);
  print("vec1", vec1);

  // fill vec2 (note: vec2 is empty!)
  std::fill(vec2.begin(), vec2.end(), 0);
  print("vec2", vec2);

  // fill_n: fill vec2 from the beginning with 10 elements of value 0
  // std::fill_n(vec2.begin(), 10, 0); // DISASTER: vec2 has zero elements!
  std::fill_n(std::back_inserter(vec2), 10, 0); // insert iterator!
  print("vec2", vec2);

  // copy
  int array[] = {0, 1, 2, 3, 4, 5};
  std::vector<int> vec3; // empty vector
  std::copy(array,array+5, std::back_inserter(vec3));
  print("vec3", vec3);

  // replace
  std::replace(vec2.begin(), vec2.end(), 0, 1); // replace every 0 by a 1 in
                                                // vec3
  print("vec2", vec2);

  // replace copy
  std::vector<int> vec4;
  std::replace_copy(vec2.begin(), vec2.end(), std::back_inserter(vec4), 1, 2);
  print("vec4", vec2);

  // operations on a random vector
  std::vector<double> vec5(5); // create vector with 5 elements
  std::generate(vec5.begin(), vec5.end(), rng()); // generate rnd vector
                                                  // using rng defined above
  print("vec5", vec5);
  // sort vec5
  std::sort(vec5.begin(), vec5.end());
  print("sorted vec5", vec5);
  // define "absolute less" function object
  struct {
    bool operator()(double a, double b) {
      return std::abs(a) < std::abs(b);
    }
  } absLess;
  // auto absLess = [](double a, double b) { return std::abs(a) < std::abs(b); };
  // use absLess as binary predicate for sort
  std::sort(vec5.begin(), vec5.end(), absLess);
  print("abs. sorted vec5", vec5);

  return 0;
}
