#!/usr/bin/env python
from argparse import ArgumentParser
import numpy as np
import matplotlib.pyplot as plt


def main():
    parser = ArgumentParser()
    parser.add_argument("input_file")
    parser.add_argument("output_file", nargs="?")
    args = parser.parse_args()

    data = np.loadtxt(args.input_file)

    fig = plt.figure()
    plt.loglog(data[:, 0], data[:, 1], label="log(N)")
    plt.loglog(data[:, 0], data[:, 2], label="N")
    plt.loglog(data[:, 0], data[:, 3], label="N*log(N)")
    plt.loglog(data[:, 0], data[:, 4], label="N**2")
    plt.loglog(data[:, 0], data[:, 5], label="N**3")
    plt.loglog(data[:, 0], data[:, 6], label="2**N")
    plt.legend()
    plt.title("Complexity")
    plt.xlabel("N")
    plt.ylabel("f(N)")

    if args.output_file:
        plt.savefig(args.output_file)
    else:
        plt.show()


if __name__ == "__main__":
    main()
