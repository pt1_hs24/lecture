#include <iostream>
#include <vector>

int main() {

  std::vector<int> iv;

  // add 24 elements to ivec, print size & capacity
  for (std::vector<int>::size_type i = 0; i <= 24; ++i) {
    std::cout << "ivec: size = " << iv.size() << " , "
              << "capacity = " << iv.capacity() << '\n';
    iv.push_back(i);
  }

  return 0;
}
