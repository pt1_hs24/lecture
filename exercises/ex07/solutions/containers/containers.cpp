#include <iomanip>   // for std::setw, std::setprecision
#include <ios>       // for std::right
#include <iostream>  // for std::cout
#include <list>      // for std::list
#include <numeric>   // for std::iota
#include <set>       // for std::set
#include <vector>    // for std::vector

#include "timer/timer.hpp"

using test_type = unsigned long;

/**
 * @brief Perform the insertion/deletion operation.
 *
 * @tparam C A container, either vector/list/set
 * @param num_ops Number of times to benchmark the operation for.
 * @param container The container storing the data.
 */
template <class C>
void perform_operation(const size_t& num_ops, C& container) {
  typename C::iterator it;

  for (size_t i = 0; i < num_ops; ++i) {
    it = container.insert(container.begin(), 0);
    container.erase(it);
  }
}

/**
 * @brief Measure the normalized time per insertion/deletion in nanoseconds.
 *
 * @tparam C A container, either vector/list/set
 * @param num_ops Number of times to benchmark the operation for.
 * @param input The input data.
 * @return double Time in nanoseconds per operation.
 */
template <typename C>
double measure_container(const size_t& num_ops,
                         const std::vector<test_type>& input) {
  Timer t;

  // C is std::vector<test_type>, std::list<test_type> or std::set<test_type>
  C container(input.cbegin(), input.cend());  // copy the input to our container

  t.start();
  perform_operation(num_ops, container);
  t.stop();

  // output in nanoseconds
  return 1e9 * t.duration() / num_ops;
}

int main() {
  const size_t num_ops = 4e06;

  std::cout << std::right << "# " << std::setw(4) << "N" << ' ' << std::setw(13)
            << "Vector[ns/op]" << ' ' << std::setw(13) << "List[ns/op]" << ' '
            << std::setw(13) << "Set[ns/op]" << '\n';
  for (unsigned i = 4; i < 14; ++i) {
    const size_t size = 1ul << i;  // == std::pow(2, i)

    std::vector<test_type> input(size);

    // Fill with increasing values 1, 2, ...
    std::iota(input.begin(), input.end(), 1);

    std::cout << std::right << std::fixed << std::setprecision(6) << "  "
              << std::setw(4) << size << ' ' << std::setw(13)
              << measure_container<std::vector<test_type>>(num_ops, input)
              << ' ' << std::setw(13)
              << measure_container<std::list<test_type>>(num_ops, input) << ' '
              << std::setw(13)
              << measure_container<std::set<test_type>>(num_ops, input) << '\n';
  }

  return 0;
}
