#include "utils.hpp"

#include <cstdlib> // rand()

namespace Penna {

// alternative to drand48 on non-POSIX systems (e.g., WIN)
// generate uniformly distributed pseudo-random numbers over the interval
// [0.0, 1.0)
double drand() {
    return static_cast<double>(rand()) / (static_cast<double>(RAND_MAX) + 1.);
}

} // namespace Penna
