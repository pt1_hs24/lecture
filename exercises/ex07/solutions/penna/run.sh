#!/bin/bash

TIME=10000      # simulational time
M=2             # mutation rate M
T=2             # bad threshold T
R=8             # reproduction age R
NMAX=10000      # population limit Nmax
N0=$((NMAX/10)) # initial population size N0 (start with 10% population)
NMEAS=$TIME     # num measurements

DATFILE=population.dat

cmake -Bbuild -H./
make -C build penna_sim
echo "Running with TIME=$TIME M=$M T=$T R=$R NMAX=$NMAX, N0=$N0..."
./build/penna_sim $TIME $M $T $R $NMAX $N0 $NMEAS > $DATFILE
gnuplot population.gnuplot
gnuplot gene_histogram.gnuplot
# ./plot.py
