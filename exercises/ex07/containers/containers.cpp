#include <iomanip>   // for std::setw, std::setprecision
#include <ios>       // for std::right
#include <iostream>  // for std::cout
#include <list>      // for std::list
#include <numeric>   // for std::iota
#include <set>       // for std::set
#include <vector>    // for std::vector

#include "timer/timer.hpp"

using test_type = unsigned long;

/**
 * @brief Perform the insertion/deletion operation.
 *
 * @tparam C A container, either vector/list/set
 * @param num_ops Number of times to benchmark the operation for.
 * @param container The container storing the data.
 */
template <class C>
void perform_operation(const size_t& num_ops, C& container) {
  // TODO: implement
}

/**
 * @brief Measure the normalized time per insertion/deletion in nanoseconds.
 *
 * @tparam C A container, either vector/list/set
 * @param num_ops Number of times to benchmark the operation for.
 * @param input The input data.
 * @return double Time in nanoseconds per operation.
 */
template <typename C>
double measure_container(const size_t& num_ops,
                         const std::vector<test_type>& input) {
  // TODO: implement
}

int main() {
  const size_t num_ops = 4e06;

  std::cout << std::right << "# " << std::setw(4) << "N" << ' ' << std::setw(13)
            << "Vector[ns/op]" << ' ' << std::setw(13) << "List[ns/op]" << ' '
            << std::setw(13) << "Set[ns/op]" << '\n';
  for (unsigned i = 4; i < 14; ++i) {
    const size_t size = 1ul << i;  // == std::pow(2, i)

    std::vector<test_type> input(size);

    // Fill with increasing values 1, 2, ...
    std::iota(input.begin(), input.end(), 1);

    std::cout << std::right << std::fixed << std::setprecision(6) << "  "
              << std::setw(4) << size << ' ' << std::setw(13)
              << measure_container<std::vector<test_type>>(num_ops, input)
              << ' ' << std::setw(13)
              << measure_container<std::list<test_type>>(num_ops, input) << ' '
              << std::setw(13)
              << measure_container<std::set<test_type>>(num_ops, input) << '\n';
  }

  return 0;
}
