cmake_minimum_required(VERSION 3.15)

project(iterators)

set(CMAKE_CXX_STANDARD 11)

if (   "${CMAKE_CXX_COMPILER_ID}" STREQUAL "GNU"
    OR "${CMAKE_CXX_COMPILER_ID}" STREQUAL "Clang")
	set (CMAKE_CXX_FLAGS "-Wall -Wextra -Wpedantic")
endif ()

# add options
option(VERBOSE "Show what's happening" OFF)
if ( VERBOSE )
  add_definitions(-DVERBOSE)
endif()
option(MOVE "Enable move ctor & assignment" ON)
if ( MOVE )
  add_definitions(-DMOVE)
endif()

add_executable(main main.cpp)
