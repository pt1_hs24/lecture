cmake_minimum_required (VERSION 3.15)
project (ex11-harmonic-oscillator)

set (CMAKE_CXX_STANDARD 11)
set (CMAKE_CXX_STANDARD_REQUIRED TRUE)
set (CMAKE_CXX_EXTENSIONS FALSE)

add_compile_options (-Wall -Wextra -Wpedantic -march=native)

find_package (LAPACK REQUIRED)
message(INFO "${LAPACK_LINKER_FLAGS} ${LAPACK_LIBRARIES}")

add_executable (harmonic_chain harmonic_chain.cpp)
target_link_options (harmonic_chain PRIVATE ${LAPACK_LINKER_FLAGS})
target_link_libraries (harmonic_chain ${LAPACK_LIBRARIES})
