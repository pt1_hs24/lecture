/*
 * Programming Techniques for Scientific Simulations I
 * Week 11
 */

#include <cmath>
#include <stdexcept>
#include <vector>
#include <iostream>

extern "C" void dsyev_(char   const & JOBZ,
                       char   const & UPLO,
                       int    const & N,
                       double       * A,
                       int    const & LDA,
                       double       * W,
                       double       * WORK,
                       int    const & LWORK,
                       int          & INFO);

std::vector<double> hamiltonian(double const K, double const m,
                                std::size_t const N) {
    std::vector<double> M(N * N);

    for (std::size_t i = 0; i < N; ++i) {
        M[i + i * N] =  2 * K / m;
        if (i != 0) {
            M[(i - 1) + i * N] = -1 * K / m;
        }
        if (i != N - 1) {
            M[(i + 1) + i * N] = -1 * K / m;
        }
    }

    return M;
}

std::vector<double> solve(std::vector<double> & M, std::size_t const N) {
    std::vector<double> omega(N);
    int info;
    double dwork;

    dsyev_('V', 'L', N, M.data(), N, omega.data(), &dwork, -1, info);
    if (info != 0) {
        throw std::runtime_error("Something went wrong!");
    }
    int lwork = static_cast<int>(dwork);

    double * const work = new double[lwork];
    dsyev_('V', 'L', N, M.data(), N, omega.data(), work, lwork, info);
    delete[] work;
    if (info != 0) {
        throw std::runtime_error("Something went wrong!");
    }

    for (auto & w : omega) {
        w = std::sqrt(w);
    }

    return omega;
}

int main() {
    double const K = 1;
    double const m = 1;
    std::size_t const N = 16;

    auto M = hamiltonian(K, m, N);
    auto omega = solve(M, N);

    std::cout << "Computed eigenvalues\n";
    for (auto const& w : omega) {
        std::cout << w << ' ';
    }
    std::cout << '\n';

    std::cout << "Exact eigenvalues\n";
    for (std::size_t i = 1; i <= N; ++i) {
        std::cout <<  std::sqrt( K/m
                                *(2. - 2.*std::cos(M_PI*static_cast<double>(i)
                                         /(N + 1))))
                  << ' ';
    }
    std::cout << '\n';

    std::cout << "Eigenvectors (stored column-wise)\n";
    for(std::size_t i = 0; i < N; ++i) {
        for(std::size_t j = 0; j < N; ++j) {
            std::cout << M[j + N*i] << ' ';
        }
        std::cout << std::endl;
    }
    std::cout << '\n';

    return 0;
}
