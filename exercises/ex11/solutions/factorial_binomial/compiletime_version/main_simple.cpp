/*
 * Programming Techniques for Scientific Simulations I
 * Week 11
 */

#include <iostream>

template<size_t N>
struct factorial {
    enum{ value =  N * factorial<N-1>::value };
};

template<>
struct factorial<0> {
    enum { value = 1 };
};

template<size_t N, size_t k>
struct binomial {
    enum{ value =  factorial<N>::value / (factorial<N-k>::value * factorial<k>::value) };
};


int main() {
    std::cout << "Factorial:" << std::endl;
    std::cout << "     5! = " << factorial<5>::value << std::endl;
    std::cout << "    20! = " << factorial<20>::value << std::endl;
    std::cout << "    21! = " << factorial<21>::value << std::endl;
    std::cout << "    70! = " << factorial<70>::value << std::endl;

    std::cout << "Binomial:" << std::endl;
    std::cout << "    N=20, k=1 ==> " << binomial<20, 1>::value << std::endl;
    std::cout << "    N=21, k=1 ==> " << binomial<21, 1>::value << std::endl;
    // will trigger a compiletime error
    // std::cout << "    N=70, k=1 ==> " << binomial<70, 1>::value << std::endl;
    return 0;
}
