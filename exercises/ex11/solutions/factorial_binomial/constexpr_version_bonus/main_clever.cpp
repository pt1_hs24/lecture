/*
 * Programming Techniques for Scientific Simulations I
 * Week 11
 */

#include <iostream>
#include <iomanip>
#include <assert.h>

// note the constexpr
constexpr size_t factorial(const size_t & value)
{
    // in a constexpr function, only a very limited set of c++ works
    // forget arrays, containers (in c++14), pointers, modifying globals, ...
    // If you can't do the same with templates only, it's not allowed.
    size_t res = 1;
    for(size_t i = 1; i <= value; ++i) {
        res *= i;
    }
    return res;
}

// note the constexpr
constexpr size_t binomial(const size_t & N, const size_t & k)
{
    size_t res = 1;
    for(size_t i = 1; i <= k; ++i) {
        res *= (N+1-i);
        assert(res % i == 0); // make sure there is no remainder (int-arithmetics)
        res /= i; // I divide after the mult, so we dont need double-arithmetics
    }
    return res;
}

constexpr unsigned long long binomial_coefficient(unsigned int n, unsigned int k) {
    if (k > n) return 0;
    if (k == 0 || k == n) return 1;
    return binomial_coefficient(n - 1, k - 1) + binomial_coefficient(n - 1, k);
}

// we only need this echo struct to demonstrate that the constexpr
// funtions can and will be executed during compiletime
// (alternative: look at assembler code)
template<size_t N>
struct echo {
    // this replaces the enum { value = value } trick
    static constexpr size_t value = N;
};


template<unsigned int N, unsigned int K>
struct BinomialCoeff {
    static constexpr unsigned long long value = 
        BinomialCoeff<N - 1, K - 1>::value + BinomialCoeff<N - 1, K>::value;
};

// Specialization for k = 0 (base case)
template<unsigned int N>
struct BinomialCoeff<N, 0> {
    static constexpr unsigned long long value = 1;
};

// Specialization for k = n (base case)
template<unsigned int N>
struct BinomialCoeff<N, N> {
    static constexpr unsigned long long value = 1;
};


template<unsigned int N, unsigned int K>
struct BinomialCoeff1 {
    static constexpr unsigned long long value = 
        std::conditional_t<
            K == 0 || K == N,
            std::integral_constant<unsigned long long, 1>,
            std::conditional_t<
                K > N,
                std::integral_constant<unsigned long long, 0>,
                std::integral_constant<unsigned long long, 
                    BinomialCoeff1<N-1, K-1>::value + BinomialCoeff1<N-1, K>::value
                >
            >
        >::value;
};

// Method 2: Using constexpr if with a helper struct
template<unsigned int N, unsigned int K, bool = (K == 0 || K == N), bool = (K > N)>
struct BinomialCoeff2Helper {
    static constexpr unsigned long long value = 
        BinomialCoeff2Helper<N-1, K-1>::value + BinomialCoeff2Helper<N-1, K>::value;
};

template<unsigned int N, unsigned int K>
struct BinomialCoeff2Helper<N, K, true, false> {
    static constexpr unsigned long long value = 1;
};

template<unsigned int N, unsigned int K>
struct BinomialCoeff2Helper<N, K, false, true> {
    static constexpr unsigned long long value = 0;
};

template<unsigned int N, unsigned int K>
struct BinomialCoeff2 {
    static constexpr unsigned long long value = BinomialCoeff2Helper<N, K>::value;
};

// Method 3: Using type traits and enable_if
template<unsigned int N, unsigned int K>
struct BinomialCoeff3 {
    template<unsigned int n, unsigned int k>
    static constexpr typename std::enable_if<
        (k == 0 || k == n),
        unsigned long long
    >::type compute() {
        return 1;
    }

    template<unsigned int n, unsigned int k>
    static constexpr typename std::enable_if<
        (k > n),
        unsigned long long
    >::type compute() {
        return 0;
    }

    template<unsigned int n, unsigned int k>
    static constexpr typename std::enable_if<
        (k != 0 && k != n && k < n),
        unsigned long long
    >::type compute() {
        return BinomialCoeff3<n-1, k-1>::value + BinomialCoeff3<n-1, k>::value;
    }

    static constexpr unsigned long long value = compute<N, K>();
};



int main()
{
    // our normal runtime code
    // (no change needed, a constexpr fct is also a normal function)
    std::cout << "Factorial:" << std::endl;
    for(size_t N = 0; N < 10; ++N) {
        std::cout << "    " << N << "! = " << factorial(N) << std::endl;
    }

    std::cout << "Binomial:" << std::endl;
    for(size_t N = 1; N < 10; ++N) {
        std::cout << "    " << std::setw(5) << "N=" << N ;
        for(size_t k = 0; k <= N; ++k) {
            std::cout << std::setw(5) << binomial(N, k);
        }
        std::cout << std::endl;
    }

    std::cout << "Limits:" << std::endl;
    std::cout << "    20! = " << factorial(20) << std::endl;
    std::cout << "    21! = " << factorial(21) << std::endl;
    std::cout << "    70! = " << factorial(70) << std::endl;

    std::cout << "Limits:" << std::endl;
    std::cout << "    N=20, k=1 ==> " << binomial(20,1) << std::endl;
    std::cout << "    N=21, k=1 ==> " << binomial(21,1) << " <- right " << std::endl;
    std::cout << "    N=70, k=1 ==> " << binomial(70,1) << std::endl;

    // using our functions in a context where the result needs to be known
    // at compiletime
    std::cout << "Compiletime Factorial:" << std::endl;
    std::cout << "    20! = " << echo<factorial(20)>::value << std::endl;
    std::cout << "    21! = " << echo<factorial(21)>::value << std::endl;
    std::cout << "    70! = " << echo<factorial(70)>::value << std::endl;

    std::cout << "Compiletime Binomial:" << std::endl;
    std::cout << "    N=20, k=1 ==> " << echo<binomial(20,1)>::value << std::endl;
    std::cout << "    N=21, k=1 ==> " << echo<binomial(21,1)>::value << std::endl;
    std::cout << "    N=70, k=1 ==> " << echo<binomial(70,1)>::value << std::endl;

    return 0;
}
