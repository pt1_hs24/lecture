/*
 * Programming Techniques for Scientific Simulations I
 * Week 11
 */

#include <cmath>
#include <stdexcept>
#include <vector>
#include <iostream>

extern "C" void dsyev_(/* TODO: Write dsyev_ declaration */);

std::vector<double> hamiltonian(double const K, double const m,
                                std::size_t const N) {
    std::vector<double> M(N * N);

    /* TODO: Set up the Hamiltonian*/

    return M;
}

std::vector<double> solve(std::vector<double> & M, std::size_t const N) {
    std::vector<double> omega(N);

    /* TODO: Compute the eigenvalues */

    return omega;
}

int main() {
    double const K = 1;
    double const m = 1;
    std::size_t const N = 16;

    auto M = hamiltonian(K, m, N);
    auto omega = solve(M, N);

    std::cout << "Computed eigenvalues\n";
    for (auto const& w : omega) {
        std::cout << w << ' ';
    }
    std::cout << '\n';

    std::cout << "Exact eigenvalues\n";
    for (std::size_t i = 1; i <= N; ++i) {
        std::cout <<  std::sqrt( K/m
                                *(2. - 2.*std::cos(M_PI*static_cast<double>(i)
                                         /(N + 1))))
                  << ' ';
    }
    std::cout << '\n';

    std::cout << "Eigenvectors (stored column-wise)\n";
    for(std::size_t i = 0; i < N; ++i) {
        for(std::size_t j = 0; j < N; ++j) {
            std::cout << M[j + N*i] << ' ';
        }
        std::cout << std::endl;
    }
    std::cout << '\n';

    return 0;
}
