/* Programming Techniques for Scientific Simulations
 * Exercise 3.4
 */

#include <iostream>            // std::cout
#include <iomanip>             // std::setprecision, std::setw, etc.
#include <vector>              // std::vector
#include <cstdlib>             // rand(), RAND_MAX

// Check if <format> is available, issue an error if not.
#ifdef __has_include
   #if __has_include(<format>)
      #include <format>        // std::format
   #else
      #error "The <format> library is not available."
   #endif
#else
   #error "The __has_include check is not available."
#endif

#define N 30

// This is a template function to generate uniform random numbers
// In a future exercise you will implement your own PRNG using
//
template<typename T>
T rand_gen() {
    return static_cast<T>(rand()) / RAND_MAX;
}

template<typename T>
void init_vector(std::vector<T>& vec) {
    for (std::size_t i = 0; i < vec.size(); ++i)
        vec[i] = rand_gen<T>();
}

template<typename Vec>
void print_vector_stream(const Vec& vec) {
    std::size_t counter = 0;
    for (auto elem : vec) {
        std::cout
            << std::fixed           // sets the floating-point output format to fixed-point notation
            << std::right           // aligns values to the right
            << std::setprecision(3) // sets the number of decimal places to 3
            << std::setw(6)         // sets the minimum width of the output field to 6 characters
            << elem << " ";

        ++counter;
        if (counter % 5 == 0) std::cout << std::endl; // split long vectors
    }
    std::cout << std::endl;
}

template<typename Vec>
void print_vector_format(const Vec& vec) {
    std::size_t counter = 0;
    for (auto elem : vec) {
        // Format the current vector element elem as a floating-point number
        // with a width of 6 characters, 3 digits after the decimal point,
        // and right-aligned. You can check the format specifications here:
        // https://en.cppreference.com/w/cpp/utility/format/formatter
        std::cout << std::format("{:>6.3f} ", elem);

        ++counter;
        if (counter % 5 == 0) std::cout << std::endl;
    }
    std::cout << std::endl;
}

int main() {
    // Seeds pseudo-random number generator
    srand(42);

    // Initialize vectors of size N with all 0s
    std::vector<double> vec_double(N, 0);
    std::vector<float>  vec_float(N, 0);

    // Replace 0s with random numbers in [0, 1)
    init_vector(vec_double);
    init_vector(vec_float);

    // Print the random vectors using streams
    print_vector_stream(vec_double);
    print_vector_stream(vec_float);

    // Print the random vectors using format
    print_vector_format(vec_double);
    print_vector_stream(vec_float);

    return 0;
}
