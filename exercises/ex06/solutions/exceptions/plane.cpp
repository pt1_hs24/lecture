#include <iostream> // for std::cout
#include <stdexcept> // for std::runtime_error

class Plane {
    public:
        void start() {
            std::cout << "Plane started successfully." << std::endl;
        }

        void serve_food() {
            throw std::runtime_error("The food is not edible!");
        }

        void land() {
            std::cout << "Plane landed successfully." << std::endl;
        }
};

int main() {
    Plane plane;
    try {
        plane.start();
        try {
            plane.serve_food();
        } catch(std::runtime_error const & e) {
            std::cout << "There was a problem with the food: " << e.what() << std::endl;
        }
        plane.land();
    } catch(...) {
        std::cout << "Mayday mayday! Performing emergency landing." << std::endl;
    }
}
