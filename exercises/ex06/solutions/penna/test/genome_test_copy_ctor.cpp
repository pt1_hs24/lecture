#include "genome.hpp"
#include <cstddef> // for std::size_t
#include <cstdlib> // for std::srand
#include <stdexcept> // for std::logic_error

using namespace Penna;

void test_copy_constructor(Genome & parent_genome) {
    Genome child_genome(parent_genome);

    // We iterate through all genes to check each gene individually.
    // Just counting the total number of bad genes is not sufficient.
    for (std::size_t age = 0; age < Genome::number_of_genes; ++age) {
        if (child_genome.count_bad(age) != parent_genome.count_bad(age))
            throw std::logic_error("Copy constructor changed the genome.");
    }
}

int main() {
    // seed the random number generator
    std::srand(42);

    // use mutation to test the copy constructor for different parent genomes
    Genome::set_mutation_rate(5);
    Genome test_genome;

    for (int i = 0; i < 10; ++i) {
        test_genome.mutate();
        test_copy_constructor(test_genome);
    }

    return 0;

}
