#!/bin/bash

# Compile all executables, one for each function to be tested.
cmake -Bbuild -H./
make -C build all

# Run all executables.
./build/simpson_benchmark_1 | tee simpson_benchmark_results.dat
./build/simpson_benchmark_2 | tee -a simpson_benchmark_results.dat
./build/simpson_benchmark_3 | tee -a simpson_benchmark_results.dat
./build/simpson_benchmark_4 | tee -a simpson_benchmark_results.dat
./build/simpson_benchmark_5 | tee -a simpson_benchmark_results.dat
./build/simpson_benchmark_6 | tee -a simpson_benchmark_results.dat

# Generate a plot.
python3 plot.py
