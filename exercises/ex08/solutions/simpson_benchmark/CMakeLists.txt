cmake_minimum_required(VERSION 3.15)

# Make it a Release build by default to add -O3 and -DNDEBUG
# Can be overridden from command line or GUI
# This code adapted from: https://blog.kitware.com/cmake-and-the-default-build-type/ 
set(default_build_type "Release")
if(NOT CMAKE_BUILD_TYPE AND NOT CMAKE_CONFIGURATION_TYPES)
  message(STATUS "Setting build type to '${default_build_type}' as none was specified.")
  set(CMAKE_BUILD_TYPE "${default_build_type}" CACHE
      STRING "Choose the type of build." FORCE)
  # Set the possible values of build type for cmake-gui
  set_property(CACHE CMAKE_BUILD_TYPE PROPERTY STRINGS
    "Debug" "Release" "MinSizeRel" "RelWithDebInfo")
endif()

project(SimpsonBenchmark)

# For Release, add -march-native (and -funroll-loops if you switch lines)
add_compile_options($<$<CONFIG:RELEASE>:-march=native>)
# add_compile_options("$<$<CONFIG:RELEASE>:-march=native;-funroll-loops>")

set(CMAKE_CXX_STANDARD 11)

add_compile_options(-Wall -Wextra -Wpedantic)

foreach(i 1 2 3 4 5 6)
  add_executable(simpson_benchmark_${i} simpson_benchmark.cpp simpson.cpp)
  target_compile_definitions(simpson_benchmark_${i} PRIVATE F${i})
endforeach()
