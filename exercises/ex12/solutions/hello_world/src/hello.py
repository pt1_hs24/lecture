#!/usr/bin/env python
# -*- coding: utf-8 -*-
#
# Programming Techniques for Scientific Simulations, ETH Zurich

def hello():
    print('Hello', end=' ')


if __name__ == '__main__':
    print('hello.py executed as main program.')
