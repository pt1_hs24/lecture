#include "matrix_multiplication.hpp"

extern "C" void dgemm_(char   const & TRANSA,
                       char   const & TRANSB,
                       int    const & M,
                       int    const & N,
                       int    const & K,
                       double const & alpha,
                       double const * A,
                       int    const & LDA,
                       double const * B,
                       int    const & LDB,
                       double const & beta,
                       double       * C,
                       int    const & LDC);

void mm_blas(matrix_t const & A,
             matrix_t const & B,
             matrix_t & C,
             std::size_t N) noexcept {
  dgemm_('N', 'N', N, N, N, 1, A.data(), N, B.data(), N, 0, C.data(), N);
}
