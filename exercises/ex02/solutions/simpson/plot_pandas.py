import matplotlib.pyplot as plt
import pandas as pd

# Note: write the results as comma-separated values (CSV). E.g. like this:
# n,value
# 1,10
# 2,14
# 3,17
# ...
data = pd.read_csv('data/results.csv', index_col='n')

data.plot()
plt.show()
