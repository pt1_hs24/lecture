#ifndef POPULATION_HPP
#define POPULATION_HPP

#include "animal.hpp"
#include "penna_vector.hpp"
#include <list>
#include <random>

namespace Penna {
/**
 * Population of animals.
 */
class Population {
    #ifdef PENNA_VECTOR
    using container_t = penna_vector<Animal>;
    #else
    using container_t = std::list<Animal>;
    #endif

public:
    using const_iterator = container_t::const_iterator;
    /**
     * Constructor.
     * @param nmax Maximum population size. Parameter N_{max} in Penna's paper.
     * @param n0 Initial population size.
     */
    Population( const size_t & nmax, const size_t & n0 );

    /// Classes with a vtable should have a virtual destructor.
    virtual ~Population();

    /// Simulate growth of the population for time years.
    void simulate( size_t time );

    /// Simulate one time step (year).
    virtual void step();

    /// Get size of population.
    std::size_t size() const;

    /// Get iterators for the population_ container.
    const_iterator begin() const;
    const_iterator end() const;

    // Option 1 (below): if population_ private we add a "getter"
    // Option 2 (below): if population_ protected we don't need a "getter"
    // Get reference to the population container
    container_t& get_population();

private:
    std::size_t nmax_;
// Option 1: keep population_ private and add "getter" above
    container_t population_;
// Option 2: make population_ protected so that derived classes (FishPopulation)
//           have access.
// protected:
//     container_t population_;
};

} // end namespace Penna

#endif // !defined POPULATION_HPP
