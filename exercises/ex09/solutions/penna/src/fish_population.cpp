#include "fish_population.hpp"
#include "utils.hpp"
#include <algorithm>
#include <functional>
#include <vector>

namespace Penna  {

FishPopulation::FishPopulation(const std::size_t nmax,
                               const std::size_t n0,
                               const double f_rate,
                               const std::size_t f_age)
    : Population(nmax, n0), f_rate_(f_rate), f_age_(f_age) {}

void FishPopulation::change_fishing(const double f_rate, const std::size_t f_age) {
    f_rate_ = f_rate;
    f_age_ = f_age;
}

class FishingPredicate {
public:
    FishingPredicate(const double probability, const std::size_t minage)
        : probability_(probability), minage_(minage) {}

    bool operator()(const Animal& a) const {
        return (a.age() > minage_) && (probability_ >= 1. || drand() < probability_);
    }

private:
    const double probability_;
    const std::size_t minage_;
};

void FishPopulation::step() {
    // Do normal aging.
    Population::step();

    // Fishing
    if(f_rate_ > 0) {
        // Option 1: population_ private in Population so we call the "getter"
        get_population().remove_if(FishingPredicate(f_rate_, f_age_));
        // Option 2: population_ protected in Population so we have access.
        // population_.remove_if(FishingPredicate(f_rate_, f_age_));
    }
}

} // end namespace Penna
