# CMake generated Testfile for 
# Source directory: /Users/levilingsch/Code/intern/hs24/exercises/ex09/solutions/penna/test
# Build directory: /Users/levilingsch/Code/intern/hs24/exercises/ex09/solutions/penna/test
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
add_test(Genome-Default-Constructor "/Users/levilingsch/Code/intern/hs24/exercises/ex09/solutions/penna/test/genome_test_default_ctor")
set_tests_properties(Genome-Default-Constructor PROPERTIES  _BACKTRACE_TRIPLES "/Users/levilingsch/Code/intern/hs24/exercises/ex09/solutions/penna/test/CMakeLists.txt;3;add_test;/Users/levilingsch/Code/intern/hs24/exercises/ex09/solutions/penna/test/CMakeLists.txt;0;")
add_test(Genome-Copy-Constructor "/Users/levilingsch/Code/intern/hs24/exercises/ex09/solutions/penna/test/genome_test_copy_ctor")
set_tests_properties(Genome-Copy-Constructor PROPERTIES  _BACKTRACE_TRIPLES "/Users/levilingsch/Code/intern/hs24/exercises/ex09/solutions/penna/test/CMakeLists.txt;7;add_test;/Users/levilingsch/Code/intern/hs24/exercises/ex09/solutions/penna/test/CMakeLists.txt;0;")
