#!/bin/bash

TIME=1600       # simulational time
M=1             # mutation rate M
T=3             # bad threshold T
R=7             # reproduction age R
P=1.0           # pregnancy probability P
NMAX=200000     # population limit Nmax
N0=$((NMAX/10)) # initial population size N0 (start with 10% population)
NMEAS=$TIME     # num measurements
M1=1000         # time at start of fishing 1
r1=0.17         # fishing rate 1
a1=0            # fishing age 1
M2=1200         # time at start of fishing 2
r2=0.22         # fishing rate 2
a2=0            # fishing age 2

DATFILE_LIST=population_list.dat
DATFILE_VEC=population_vec.dat

cmake -Bbuild -H./
make -C build
mkdir -p result
echo "Timing with TIME=$TIME M=$M T=$T R=$R P=$P NMAX=$NMAX, N0=$N0, " \
                 "M1=$M1, r1=$r1, a1=$a1, M2=$M2, r2=$r2, a2=$a2 ..."
echo "List version:"
time -p ./build/penna_sim $TIME $M $T $R $P $NMAX $N0 $NMEAS $M1 $r1 $a1 \
                          $M2 $r2 $a2                                    \
                          > result/$DATFILE_LIST
echo ""
echo "Penna vector version:"
time -p ./build/penna_sim_vector $TIME $M $T $R $P $NMAX $N0 $NMEAS $M1 $r1 \
                                 $a1 $M2 $r2 $a2                            \
                                 > result/$DATFILE_VEC
python3 plot.py
